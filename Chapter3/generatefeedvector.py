import feedparser
import re

# Returns title and dictionary of word counts for an RSS feed
def getwordcounts(url):
    # Parse the feed
    parsedURL = feedparser.parse(url)
    wordcount = {}
    
    # Loop over all the entries
    #   ENTRIES doc: A list of dictionaries. Each dictionary contains data from
    #       a different entry. Entries are listed in the order in which
    #       they appear in the original feed. 
    #   Tip: This element always exists, although it may be an empty list.
    for entry in parsedURL.entries:
        if 'summary' in entry: summary = entry.summary
        else: summary = entry.description

        # Extract a list of words (getwords defined below)
        words = getwords(entry.title + ' ' + summary)

        # Construct the dictionary of wordcounts
        for word in words:
            wordcount.setdefault(word, 0)
            wordcount[word] += 1
    return parsedURL.feed.title, wordcount


def getwords (html):
    # Remove all the HTML tags (i.e. replace with '')
    txt = re.sub(r'<[^>]+}', '', html)

    # Split words by all non-alpha characters
    words = re.compile(r'[^A-Z^a-z]+').split(txt)
    
    # Convert to lowercase
    return [word.lower() for word in words if word is not '']


# loop over the feeds and generate the dataset
#  WORDCOUNTS: #each word in total
#  APPEARANCECOUNTS: #blogs each word occurs in
appearancecounts = {}
wordcounts = {}
length = 0
with open('feedlist.txt', 'r') as feedlist:
    for feedURL in feedlist:
        length += 1
        title, wordcount = getwordcounts(feedURL)
        wordcounts[title] = wordcount
        for word, count in wordcount.items():
            appearancecounts.setdefault(word, 0)
            if count > 1:
                appearancecounts[word] += 1

    # Select only the words used in between 10% and 60% of blogs
    wordlist = []

    for word, blogcount in appearancecounts.items():
        fraction = float(blogcount) / length
        if 0.1 < fraction < 0.6: wordlist.append(word)


    # Create textfile containing matrix of all word counts for each blog
    with open('blogdata.txt','w') as out:
        out.write('blog')
        for word in wordlist: out.write('\t%s' % word)
        out.write('\n')
        for blog, wordcount in wordcounts.items():
            out.write(blog)
            for word in wordlist:
                if word in wordcount: out.write('\t%d' % wordcount[word])
                else: out.write('\t0')
            out.write('\n')

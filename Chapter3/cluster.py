# Each cluster is either a point in the tree with two branches, or an endpoint
#  associated with a row from the dataset (i.e. blog).
# Each cluster contains data about its location; either the row of its data
#  or merged data from its to branches

class bicluster:
    def __init__(self, vec, left=None, right=None, distance=0.0, id=None):
        self.left = left
        self.right = right
        self.vec = vec
        self.id = id
        self.distance = distance

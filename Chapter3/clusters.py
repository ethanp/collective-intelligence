
# *@* HIERARCHICAL CLUSTERING *@*

#  ALGOGITHM SPECIFICS/BASICS:
#   Count instances of each word and number of blogs each word appears in
#   If it appears in 10-60% of blogs, make it a column of a table
#       Make each blog a row of the table
#       So the table has the count of each word in each blog
#   Now say that each row of word-counts-in-a-blog is a vector
#       so you've got a whole bunch of these vectors, where the value of each
#       dimension corresponds to the number of times that word appears
#   Calculate the Pearson-Correlation ("r") between each vector;
#       basically you're seeing if the Relative Proportions of different
#       words is the same pair-wise across blogs.
#           In my opinion, this is a silly way of finding "distance",
#               but it certainly is simple
#   For the pair of blogs with the highest Correlation, group them
#       and average their "vectors", word-by-word, making a new vector
#   Repeat until you have a tree of pairwise groupings
#
#   I'm not sure yet what one can do with this tree besides analyze it visually

# **THERE'S SOME BUG IN HERE**, because this shit's NOT WORKING out right

# From blogdata.txt (table of words frequency in blogs):
#   use Hierarchical Clustering to determine which feeds are similar

def readfile(filename):
    lines = [line for line in file(filename)]

    # First line contains column titles
    colnames = lines[0].strip().split('\t')[1:]
    rownames = []
    data = []
    for line in lines[1:]:
        vals = line.strip().split('\t')[1:]
        # First col is rowname
        rownames.append(vals[0])
        # Rest is datavals
        data.append([float(x) for x in vals[1:]])
    return rownames, colnames, data


# Use Pearson to define Closeness, corrects for fact that some
#   blogs contain more words than others
def pearson(v1,v2): # (vectors)
    sum1 = sum(v1)
    sum2 = sum(v2)

    # Sums of Squares
    sum1Sq = sum([v**2 for v in v1])
    sum2Sq = sum([v**2 for v in v2])

    # Sum of products
    pSum = sum([v1[i]*v2[i] for i in range(len(v1))])

    # Calculate 'r'
    numerator = pSum - (sum1 * sum2 / float(len(v1)))
    denominator = ((sum1Sq - sum1**2/float(len(v1))) * 
                   (sum2Sq - sum2**2/float(len(v1))))**(1/2)
    if denominator==0: return 0
        
    # inverted because we want More similar vectors to have Lower distance 
    return 1 - numerator/denominator


# Each cluster is either a point in the tree with two branches, or an endpoint
#  associated with a row from the dataset (i.e. blog).
# Each cluster contains data about its location; either the row of its data
#  or merged data from its to branches

class bicluster:
    def __init__(self, vec, left=None, right=None, distance=0.0, id=None):
        self.left = left
        self.right = right
        self.vec = vec
        self.id = id
        self.distance = distance


# First make each item a cluster, then continually calculate the correlation
#  between each pair and group the two closest. The new cluster's data is
#  the avg of the old two. Stop when one cluster remains.
# This algorithm involves a lot of recalculation of these pairwise-
#  correlations, so it may take some time.

def hcluster(rows, distance=pearson):
    distances = {}
    currentclustid = -1
    # I can't figure out the Point of this 'currentclustid' variable!

    # Clusters are initially just the rows
    clust = [ bicluster(rows[i], id=i) for i in range(len(rows)) ]

    while len(clust) > 1:
        lowestpair = (0,1)
        closest = distance(clust[0].vec, clust[1].vec)

        # loop through every pair looking for the smallest distance
        for i in range(len(clust)):
            for j in range(i+1,len(clust)):
                # distances is the cache of distance calculations
                if (clust[i].id, clust[j].id) not in distances:
                    distances[ (clust[i].id, clust[j].id) ] = distance(clust[i].vec, clust[j].vec)

                # This seems odd, but it's necessary because it was only a local var before
                d = distances[ (clust[i].id, clust[j].id) ]

                if d < closest:
                    closest = d
                    lowestpair = (i,j)

        # calculate the (col-by-col) avg of the two clusters
        mergevec = [
                        ( 
                            clust[lowestpair[0]].vec[i] +
                            clust[lowestpair[1]].vec[i]
                        ) / 2.0
                      for i in range (len(clust[0].vec))
                   ]

        # create the new cluster
        newcluster = bicluster( mergevec, 
                                left     = clust[lowestpair[0]],
                                right    = clust[lowestpair[1]],
                                distance = closest,
                                id       = currentclustid )

        # cluster ids that weren't in the original set are negative
        currentclustid -= 1
        # These HAVE to be out of order like that because there are two,
        #  so if you delete [0] first, there won't EXIST a [1],
        #  so you could do [0],[0] , but that would be less clear.
        del clust[lowestpair[1]]
        del clust[lowestpair[0]]
        
        clust.append(newcluster)

    return clust[0]


# Traverse the tree and print the hierarchy
def printclust(clust, labels=None, n=0):
    # indent to make a hierarchy layout
    for i in range(n): print ' ',
    if clust.id < 0:
        # negative id means this is a branch
        print '-'
    else:
        # positive id means that this is an endpoint
        if labels == None: print clust.id
        else: print labels[clust.id]

    # now print the right and left branches
    if clust.left is not None: printclust(clust.left, labels=labels, n=n+1)
    if clust.right is not None: printclust(clust.right, labels=labels, n=n+1)


# Transpose the Data-Matrix
def rotatematrix(data):
    newdata = []
    for i in range(len(data[0])):
        newrow = [ data[j][i] for j in range(len(data)) ] 
        newdata.append(newrow)
    return newdata


# *@* K-MEANS CLUSTERING *@*

# ALGORITHM: you tell it how many clusters to make, it determins the size of the clusters
#   Start with 'k' randomly placed CENTROIDS
#   Assign each item to its nearest Centroid
#   Repeat:
#       Move each centroid to the avg location of the items assigned to it
#       Reassign each item to its nearest Centroid
#       if convergence is reached, stop

import random

def kcluster(rows, distance=pearson, k=4):
    
    # Determine the minimum and maximum values for each point
    ranges=[    (
                min([row[i] for row in rows])
                ,
                max([row[i] for row in rows])
                )
            for i in range(len(rows[0]))
           ]
    
    # Create k randomly placed centroids
        # Jeez! Ain't this a nasty little Perl of Python?
        #  It looks like EACH ROW has
        #   4 Centroids randomly placed between that row's Min&Max values
        #  That's actually right, because a random Centroid in N-dimensions
        #    would have a random value headed in each dimension
    clusters=[    
                [
                    random.random() * ( ranges[i][1] - ranges[i][0] )
                    +
                    ranges[i][0]
                  for i in range(len(rows[0]))
                ]
              for j in range(k)
             ]
    lastmatches=None
    for t in range(100):
        print 'Iteration %d' % t
        bestmatches = [ [] for i in range(k)]
        
        # Find which centroid is the closest for each row
        for j in range(len(rows)):
            row = rows[j]
            bestmatch = 0
            for i in range(k):
                d = distance(clusters[i],row)
                if d < distance(clusters[bestmatch],row):
                    bestmatch = i
            bestmatches[bestmatch].append(j)
        
        # If the results are the same as last time, this is complete
        if bestmatches == lastmatches: break
        lastmatches = bestmatches

        # Move the centroids to the average of their members
        # for each centroid
        for i in range(k):
            avgs = [0.0] * len(rows[0])
            if len(bestmatches[i]) > 0:
                # for each row
                for rowid in bestmatches[i]:
                    # for each column
                    for m in range(len(rows[rowid])):
                        # Accumulate for this row
                        avgs[m] += rows[rowid][m]
                # now divide each rows accumulation by its number of columns
                for j in range(len(avgs)):
                    avgs[j] /= len(bestmatches[i])
                # save that in THIS ("i") Centroid's array-slot
                #   as the new location
                clusters[i] = avgs
    # return the whole array of new locations
    return bestmatches

